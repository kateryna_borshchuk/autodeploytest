﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestSIte.Startup))]
namespace TestSIte
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
